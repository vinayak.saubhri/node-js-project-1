const mongoose = require("mongoose");
const { Schema } = mongoose;

const CatSchema = new Schema({
  Name: {
    type: String,
    required: [true, "Must Provide Name"],
    trim: true,
    maxlength: [20, "Name can not be more than 20 characters"],
  },
  Age: {
    type: Number,
    required: [true, "Must Provide Age"],
  },
  Breed: {
    type: String,
    required: [true, "Must Provide Breed"],
    trim: true,
    maxlength: [20, "Name can not be more than 20 characters"],
  },
});

module.exports = mongoose.model("Cat", CatSchema);
