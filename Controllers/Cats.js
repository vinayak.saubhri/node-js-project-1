const Cat = require("../Models/Cats");
const getAllCat = async (req, res) => {
  try {
    const cat = await Cat.find({});
    res.status(200).json({ cat });
  } catch (error) {
    res.status(500).json({ msg: error });
  }
};

const createCat = async (req, res) => {
  try {
    console.log(req.body);
    const cat = await Cat.create(req.body);
    res.status(201).json({ Cat });
  } catch (error) {
    res.status(500).json({ msg: error });
  }
  // res.json({ msg: "send" });
};
const getCat = async (req, res) => {
  try {
    const { id: CatId } = req.params;
    const cat = await Cat.findOne({ _id: CatId });

    if (!cat) {
      return res.status(404).json({ msg: `No Cat with id : ${CatId}` });
    }
    res.status(200).json({ cat });
  } catch (error) {
    res.status(500).json({ msg: error });
  }
};
const updateCat = async (req, res) => {
  try {
    const { id: CatId } = req.params;
    // res.status(200).json({ id: CatId, data: req.body });
    const cat = await Cat.findOneAndUpdate({ _id: CatId }, req.body, {
      new: true,
      runValidators: true,
    });

    if (!cat) {
      return res.status(404).json({ msg: `No Cat with id : ${CatId}` });
    }
    res.status(200).json({ cat });
  } catch (error) {
    res.status(500).json({ msg: error });
  }
};
const deleteCat = async (req, res) => {
  try {
    const { id: CatId } = req.params;
    const cat = await Cat.findOneAndDelete({ _id: CatId });
    if (!cat) {
      return res.status(404).json({ msg: `No Cat with id : ${CatId}` });
    }
    res.status(200).json({ cat });
  } catch (error) {
    res.status(500).json({ msg: error });
  }
};

const searchCat = async (req, res) => {
  try {
    const cat = await Cat.find({
      Age: { $gte: req.query.age_lte, $lte: req.query.age_gte },
    });
    res.status(200).json({ cat });
  } catch (error) {
    res.status(500).json({ msg: error });
  }
  // res.json({ msg: "send" });
};
module.exports = {
  getAllCat,
  createCat,
  getCat,
  updateCat,
  deleteCat,
  searchCat,
};
