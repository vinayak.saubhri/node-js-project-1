const express = require("express");
const app = express();
const cats = require("./Routes/Cats");
const PORT = 4000;
const connectDB = require("./DB/Connect");
require("dotenv").config();

//middleware
app.use(express.json());
// routes
app.get("/hello", (req, res) => {
  res.send("Task Manager App");
});

app.use("/api/v1/cats", cats);

const start = async () => {
  try {
    await connectDB(process.env.MONGO_URI);
    app.listen(PORT, console.log(`Server is listening on port ${PORT}....`));
  } catch (err) {
    console.log(err);
  }
};

start();
